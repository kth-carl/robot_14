#!/bin/bash

# A script to automate installation and autostart of balena wifi-connect
# Sixten Norelius
# EH1010 Elektroprojekt 1
# KTH
# 2021-05-01
# Fetches and installs wifi-connect
bash <(curl -L https://github.com/balena-io/wifi-connect/raw/master/scripts/raspbian-install.sh)

echo 'Moving service'
sudo mv wifi-connect-start.service /lib/systemd/system/wifi-connect-start.service
echo 'Enabling service'
sudo systemctl enable wifi-connect-start.service
echo 'Please reboot for settings to take effect'
