


//#include <AccelStepper.h>
#include <VariableTimedAction.h>



// Define pin connections
#define dirPin 2
#define stepPin 3
#define sleepPin 4
#define stepsPerRevolution 1600 // 200 native steps per rotation times 8 microsteps



// ???????


// PROGRAM CONSTANTS
char incomingByte;
bool motor_on = false;
int input_data;

int rpm = 0;
int PWM_delay = 0;

struct motor_data_t {
    int start_int;
    int rpm;
    int end_ind;
} motor_data ={0x40, 0, 0x50};


// Constants
const int motor_data_t_size = sizeof(motor_data); // Struct size
const int BAUD_RATE = 28800;
const int RX_READ_DELAY = 100;
const int TX_WRITE_DELAY = 1000;
const int ROTATE_MOTOR_DELAY = 1;


void send_motor_data() {
    /*
    Sends the motor data

    */

    // Write the struct
    Serial.write((byte*)&motor_data, motor_data_t_size);
    
    // BLOCK until the entire message is written to the serial bus
    Serial.flush(); 

}


void set_PWM_delay() {

    /*
    Sets the PWM delay which directly relates to the rpm
    */
    if (rpm != 0) {
        PWM_delay = 18750 / rpm; 
    }
}


void rotate_motor() {
    /*
    Rotate the motor one revolution
    */

    if (rpm == 0) {
        return;
    }

    // Update the motor speed
    set_PWM_delay();
    
    // Spin the stepper motor 1 revolution slowly:
    for (int i = 0; i < stepsPerRevolution; i++) {
        
        // These four lines result in 1 step:
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(PWM_delay);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(PWM_delay);

    }

}


void generate_sensor_data() {
    /*
    Generate the sensor data
    */
    
    // If motor is off
    // if (rpm == 0 && motor_on == false) {
    //     return; 
    // }
    
    // Update all data in the struct
    motor_data.rpm = rpm;

    // Send sensor data
    send_motor_data();

}


void motor_control_serial(int speed) {
    /*
    Change the motorspeed
    */

   // ParseInt defaults to 0
   if (speed == -2) {
       speed = 0;
   }
    
    // If in valid range
    if (speed >= 0 && speed <= 300 ){

        // Set rpm if given a valid range
        rpm = speed;

        // We need to wait atleast 1 ms before issuing a step
        if (rpm != 0) {
            // delay(1);
            motor_on = true;
        }

        // Turn off motor
        if (rpm == 0) {

            // Update all data in the struct
            motor_data.rpm = rpm;
            motor_on = false;

            // Send sensor data
            send_motor_data();

            return;

        }

        // Update the motor speed
        set_PWM_delay();

    // Else we just return
    } else {
        return;
    }

}



void serialFlush(){
    /*
    Empties the serial buffer by reading all bytes in the buffer
    */
    while(Serial.available() > 0) {
        incomingByte = Serial.read();
    }
}   


class Motor : public VariableTimedAction {
  public:
  unsigned long run() {
    rotate_motor();
    return 0;
  }
};


class RX_Manager : public VariableTimedAction {
  public:
  unsigned long run() {
    
    // If there is data available from the user
    while (Serial.available() > 0) {   

        input_data = Serial.parseInt();

        if (input_data != 0) {

            // Send the new recivied data to update the motor_value
            motor_control_serial(input_data);

            // Generate motor_data
            generate_sensor_data();

        }      
    }
    return 0;
  }
};


class TX_Manager : public VariableTimedAction {
  public:
  unsigned long run() {
    generate_sensor_data();
    return 0;
  }
};


Motor motor_deamon;
RX_Manager rx_deamon;
TX_Manager tx_deamon;


void setup() {
  
    // Start serial at given BAUD_RATE
    Serial.begin(BAUD_RATE);

    // Generate random SEED from the A1 port
    randomSeed(analogRead(A1));

    // Declare pins as output:
    pinMode(stepPin, OUTPUT);
    pinMode(dirPin, OUTPUT);

    // Set Pin as HIGH
    digitalWrite(dirPin, HIGH);

    generate_sensor_data();

    // Start the deamons
    rx_deamon.start(RX_READ_DELAY);
    tx_deamon.start(TX_WRITE_DELAY);
    motor_deamon.start(ROTATE_MOTOR_DELAY);

}


// Loopy loop
void loop() {

    VariableTimedAction::updateActions();

}
