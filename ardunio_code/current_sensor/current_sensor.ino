#include <Adafruit_INA260.h>

Adafruit_INA260 ina260 = Adafruit_INA260();

int sensor=A0;  //Analog 0 pin named as sensor
int output=9;   //Pin-9 is declared as output


void setup() {
pinMode(output, OUTPUT); //Pin-9 is declared as output 

  Serial.begin(115200);
  // Wait until serial port is opened
  while (!Serial) { delay(10); }

  Serial.println("Adafruit INA260 Test");

  if (!ina260.begin()) {
    Serial.println("Couldn't find INA260 chip");
    while (1);
  }
  Serial.println("Found INA260 chip");
}

void loop() {
  Serial.print("Current: ");
  Serial.print(ina260.readCurrent());
  Serial.println(" mA");

  Serial.print("Bus Voltage: ");
  Serial.print(ina260.readBusVoltage());
  Serial.println(" mV");

  Serial.print("Power: ");
  Serial.print(ina260.readPower());
  Serial.println(" mW");

    int reading=analogRead(sensor); //Reading the voltage out by potentiometer
    int bright=reading/4;           //Dividing reading by 4 to bring it in range of 0 - 255                               
                        //Delay is not necessary you can remove it
    analogWrite(output, bright);    //Finally outputting the read value on pin-9 fading led

  Serial.println();
  delay(1000);
  
}
