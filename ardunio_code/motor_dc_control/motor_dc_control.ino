
#include <Adafruit_INA260.h>

Adafruit_INA260 ina260 = Adafruit_INA260();



// Motor B connections
const short enB = 3;
const short in3 = 5;
const short in4 = 4;

// LED Connection
const int sensor = A0;  //Analog 0 pin named as sensor
const int led_output = 9;   //Pin-9 is declared as output
int reading_led;


bool motor_on = false;
bool change_dir = false;


// PROGRAM CONSTANTS
char incomingByte;
const int BAUD_RATE = 28800;
int speed_motor = 30;
int input_data;

struct motor_data_t {
    int start;
    int speed_motor;
    float motor_voltage;
    float motor_current;
    float motor_power;
    float fluid_viscosity;
    float motor_rpm;
    int end;
} motor_data_test={0x40, 0,0,0,0,0,0, 0x50};

// Struct size
const int motor_data_t_size = sizeof(motor_data_test);


void setup() {
  
    // Start serial at given BAUD_RATE
    Serial.begin(BAUD_RATE);

    // Generate random SEED
    randomSeed(analogRead(A1));

    // Set all the motor control pins to outputs
    pinMode(enB, OUTPUT);
    pinMode(in3, OUTPUT);
    pinMode(in4, OUTPUT);

    // Set LED output
    pinMode(led_output, OUTPUT); //Pin-9 is declared as output 

    // Turn off motors - Initial state
    digitalWrite(in3, LOW);
    digitalWrite(in4, LOW); 
    motor_on = false;

    while (!Serial) { delay(10); }
     Serial.println("Adafruit INA260 Test");
        if (!ina260.begin()) {
            Serial.println("Couldn't find INA260 chip");
            while (1);
        }
    
    Serial.println("Found INA260 chip");

    // Generate sensor data
    generate_sensor_data();

}


void send_senor_data() {

    // Write the struct
    Serial.write((byte*)&motor_data_test, motor_data_t_size);
    // BLOCK until the entire message is written to the serial bus
    Serial.flush(); 

}


void generate_sensor_data() {

    // if (speed_motor == 0) {
    //     return; 
    // }
        
    // Init
    // TMP : Uncessary memory
    float motor_voltage = 0;
    float motor_current = 0;
    float motor_power = 0;
    float motor_rpm = 0;
    float fluid_viscosity = 0;

    float rpm_rand = (float)random(-10, 10); // -10 - 10
    float motor_voltage_rand = (float)random(-250, 250) / 1000; // -0.25 - 0.25
    float motor_current_rand = (float)random(-100, 100) / 1000; // -0.1 - 0.1
    float fluid_viscosity_rand = (float)random(-20, 20) / 10; // -0.2 - 0.2

    // Calc motor rpm
    float motor_speed_scale = (float)speed_motor / 255;

    // Generate all the data...
    motor_rpm = motor_speed_scale * 1000 + rpm_rand; // Max: 1010, Min: -10
    motor_voltage = motor_speed_scale * 12 + motor_voltage_rand;
    motor_current = motor_speed_scale + motor_current_rand;
    motor_power = motor_voltage * motor_current;
    fluid_viscosity = (motor_power / 10) + fluid_viscosity_rand;

    // use the sensor
    motor_voltage = ina260.readBusVoltage() / 1000;
    motor_current = ina260.readCurrent() / 1000;
    motor_power = ina260.readPower() / 1000;

    // Update all data in the struct
    motor_data_test.motor_rpm = motor_rpm;
    motor_data_test.speed_motor = speed_motor;
    motor_data_test.fluid_viscosity = fluid_viscosity;
    motor_data_test.motor_voltage = motor_voltage;
    motor_data_test.motor_current = motor_current;
    motor_data_test.motor_power = motor_power;
    

    send_senor_data();
    // // Write the struct
    // Serial.write((byte*)&motor_data_test, motor_data_t_size);
    // // BLOCK until the entire message is written to the serial bus
    // Serial.flush(); 

}


void motor_control_serial(int speed) {
    
    if (speed >= -2 && speed <= 255){

        if (speed == -2) {
            speed = 0;
        }

        // Set speed_motor
        speed_motor = speed;

        // Start motor if speed != 0
        if (motor_on == false && speed != 0) {
          digitalWrite(in3, LOW);
          digitalWrite(in4, HIGH);
          motor_on = true;          
        }
        
        // Shutdown motor
        if (speed == 0){
            digitalWrite(in3, LOW);
            digitalWrite(in4, LOW);
            return;         
        }
        
        // Change direction
        if (speed == -1) {
          if (change_dir == false) {
            digitalWrite(in3, HIGH);
            digitalWrite(in4, LOW);
            change_dir = true;            
          } else {
            digitalWrite(in3, LOW);
            digitalWrite(in4, HIGH);
            change_dir = false;                 
          }
          return;
        }

        
        analogWrite(enB, speed_motor); //sets the motors speed
        // send_senor_data();
        // delay(10);

    } else {
        return;
    }

}



void serialFlush(){
    while(Serial.available() > 0) {
        incomingByte = Serial.read();
    }
}   


// Loopy loop
void loop() {

    while (Serial.available() > 0) {   

        input_data = Serial.parseInt();

        if (input_data != 0) {

            // Send the new recivied data to update the motor_value
            motor_control_serial(input_data);

            // Generate motor_data
            generate_sensor_data();

        }      

        // // Clear
        // serialFlush();

    }

    // Generate sensor data
    generate_sensor_data();
    // serialFlush();



    reading_led = analogRead(sensor) / 4; //Reading the voltage out by potentiometer       //Dividing reading by 4 to bring it in range of 0 - 255                               
    analogWrite(led_output, reading_led);    //Finally outputting the read value on pin-9 fading led

    // Once every second
    delay(1000);

}

