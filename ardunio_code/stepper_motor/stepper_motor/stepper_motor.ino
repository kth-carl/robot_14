// Include the AccelStepper Library
#include <Adafruit_INA260.h>

Adafruit_INA260 ina260 = Adafruit_INA260();

// Define pin connections
#define dirPin 2
#define stepPin 3
#define stepsPerRevolution 200

int RPM = 70;
int PWM_delay = 150000 / (RPM * 16);


int sensor=A0;  //Analog 0 pin named as sensor


void setup() {


    Serial.begin(9600);
    // Declare pins as output:
    pinMode(stepPin, OUTPUT);
    pinMode(dirPin, OUTPUT);



    Serial.print("RPM : ");
    Serial.print(RPM, DEC);
    Serial.print("    PWM DELAY : ");
    Serial.print(PWM_delay, DEC);
    Serial.print("\n");

  Serial.println("Adafruit INA260 Test");
  if (!ina260.begin()) {
    Serial.println("Couldn't find INA260 chip");
    while (1);
  }
  Serial.println("Found INA260 chip");

}

void print_current() {
  Serial.print("Current: ");
  Serial.print(ina260.readCurrent());
  Serial.println(" mA");

  Serial.print("Bus Voltage: ");
  Serial.print(ina260.readBusVoltage());
  Serial.println(" mV");

  Serial.print("Power: ");
  Serial.print(ina260.readPower());
  Serial.println(" mW");
}

void rotate_motor() {
    // Set the spinning direction clockwise:
    digitalWrite(dirPin, HIGH);
    // Spin the stepper motor 1 revolution slowly:
    for (int i = 0; i < stepsPerRevolution; i++) {
        // These four lines result in 1 step:
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(PWM_delay);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(PWM_delay);
    }

}

void loop() {

    print_current();
    rotate_motor();

}
