
import time
import threading
import queue
import serial.tools.list_ports
from flask_socketio import SocketIO
from flask_socketio import send, emit
import pprint
import sys


import serial
import aioserial
import asyncio
from struct import *


# CONSTANTS
# SERIAL_NUMBER = "75335313437351E04282" # Old arduino
# Serial number for the arduino given by KTH
SERIAL_NUMBER = "85937313737351519141"  

BAUD_RATE = 28800
STRUCT_SIZE = 6
verbose_motor_data = True


def parse_motor_data(parsed_serial_data):
    '''
    Parses the serial data from the arduino as motor_data

    Input
    ---------
    serial_data : Bytes
        Byte encoded string

    Return
    ---------
    motor_data_dict : dict
        Dictionary with the motor_data

    OR

    "NO_DATA" string if the parsing was unsuccesful
    '''

    motor_data = {}

    try:

        motor_data["motor_status"] = "PÅ"
        motor_data["rpm"] = int(parsed_serial_data[1])
        motor_data["timestamp"] = time.time() * 1000

    except Exception as e:

        print("EXCEPT", e)
        return None

    return motor_data


def find_ardunio(sel_port):
    '''
    Given a port, it checks if the serial number of the Arduino is present
    and IF it is, it returns the address of the serial device

    Input
    ---------
    sel_port : serial.list_ports
        Selected port

    Return
    ---------
    arduino_port : string
        Address of the serial device
    '''

    for port in sel_port:
        if SERIAL_NUMBER in port:
            arduino_port = sel_port[0]
            print("port", arduino_port)
            return arduino_port

    return None


def search_ports():
    '''
    Searches for serial ports that has a serial device connected to it,
    if it finds one, it connects to it!

    Return
    ---------
    arduino_port : string
        Address of the serial device

    ser : pyserial.serial
        Serial object (serial connection to the device)
    '''

    myports = [tuple(p) for p in list(serial.tools.list_ports.comports())]
    arduino_port = None  # Assume no connection
    found = False

    for sel_port in myports:

        arduino_port = find_ardunio(sel_port)

        # Connect to the arduino and Stop if we find it
        if arduino_port != None:
            found = True
            break

    if found == False:
        print("Arduino not found!")

    return arduino_port


async def connect_to_arduino(socketio, q):

    # Initial scan
    print("Starting scan!")
    arduino_port = search_ports()

    print(f"Connecting to: {arduino_port}")
    aioserial_instance = aioserial.AioSerial(port=arduino_port, baudrate=BAUD_RATE)
    print("Connection established!")

    buf = bytearray()

    new_data = False
    reciv_new_data = False
    start_marker = False
    end_marker = False
    verbose_motor_data = True
    i = 0
    updated_power_to_motor = True

    while True:

        try:
            # Read serial data
            binary_serial_data = await aioserial_instance.read_async()

            # Extend the bytearray if we want to recive data!
            buf.extend(binary_serial_data)
            i += 1

            # START INT
            if (len(buf) == 2) and start_marker == False:
                if (int.from_bytes(buf[0:2], "little")) == 0x40:
                    start_marker = True

            # END INT
            if (len(buf) == STRUCT_SIZE) and end_marker == False:
                if (int.from_bytes(buf[-2:-1], "little")) == 0x50:
                    end_marker = True

            # We can now parse the data!
            if start_marker == True and end_marker == True:

                # Parse the bytearray as as struct
                parsed_serial = unpack('hhh', buf)
                motor_data = parse_motor_data(parsed_serial)

                # Skip the iteration
                if motor_data == None:
                    break

                # Send out the new motor_data to all clients
                socketio.emit('request_motor_data', motor_data, broadcast=True)

                if verbose_motor_data == True:
                    print("--------------------\n")
                    print(time.ctime())
                    pprint.pprint(motor_data)
                    print("--------------------\n")

                # Reset!
                buf = bytearray()
                start_marker = False
                end_marker = False

            # Reset!
            if len(buf) > 2 and start_marker == False:
                buf = bytearray()  # Reset
                end_marker = False

            # Reset!
            if len(buf) > STRUCT_SIZE and end_marker == False:
                buf = bytearray()  # Reset
                start_marker = False

            # Send new POWER_TO_MOTOR commands
            # print("QUEUE empty: ", q.empty(), "updated_power_to_motor", updated_power_to_motor)
            if q.empty() == False:

                if verbose_motor_data == True:
                    print("Queue len: ", q.qsize())

                # Get power_to_motor value
                power_to_motor = q.get()

                # Encode the command
                power_to_motor = (str(power_to_motor) + "\n").encode('utf_8')

                # Send it to the arduino
                print("Sending pwr_to_moro:", power_to_motor)

                # Write to arduino
                await aioserial_instance.write_async(power_to_motor)

        except KeyboardInterrupt:
            print("Keyboard interrupt!")
            break

        except Exception as e:
            print("Random exepction!: ", e)
            # Stop if disconnected
            arduino_port = search_ports()
            if arduino_port == None:
                print("Arduino disconnected")
                time.sleep(5)
                continue


def start_stuff(socket, q, sleep_delay=5):

    asyncio.run(connect_to_arduino(socket, q))
