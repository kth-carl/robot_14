
import random
import threading
import os
import pprint
import queue
import eventlet
import time
import queue

from contextlib import suppress
import asyncio
from flask import Flask, render_template, request, jsonify
from flask import render_template, Blueprint
from flask import request
from flask_socketio import SocketIO
from flask_socketio import send, emit
from flask_cors import CORS

from ardunio_code import arduino_connect

# Socket IO och Flask misc
statusQueue = eventlet.Queue()
app = Flask(__name__)
socketio = SocketIO(app)
CORS(app)
thread = None
verbose = False

# Blueprint
main = Blueprint(
    'main',
    __name__,
    template_folder='templates',
    url_prefix='/'
)

# Queue
global q
q = queue.Queue()

time_now = time.time()
motor_data = {
    "timestamp": time_now,
    "motor_rpm": 0,
}

MIN_RPM = 0
MAX_RPM = 255

@app.route('/')
def render_home_page():
    return render_template('index.html')

@socketio.on('connect')
def connect():
    emit('Client connected')

@socketio.on('disconnect')
def disconnect():
    print('Client disconnected')

# På "request_motor_data" --> Skicka motordata genom "recive_motor_data"
@socketio.on('request_motor_data')
def request_motor_data():
    emit('recive_motor_data', motor_data)

# Send motor power
@socketio.on('send_motor_power')
def add_motor_command_to_queue(data):

    # Check if it is a int
    try:

        # Update motor_power
        # This is needed to update the value of the variable
        new_rpm = int(data)

        if new_rpm > MAX_RPM or new_rpm < MIN_RPM:
            return

    except:
        return

    # Make sure the queue is global
    global q

    if verbose == False:
        print('Recived new motor power', new_rpm)
    
    # To aviod Serial.parseInt() which seems to default to 0
    if new_rpm == 0:
        new_rpm = -2

    # Put the rpm in a event_queue
    q.put(new_rpm)

# Get motor power on connect
@socketio.on('get_motor_power')
def get_motor_power():
    emit("recive_motor_power", rpm)


@socketio.on('init_connect')
def init_connection():
    emit("init_connect_data", motor_data)


# Thread for simulating motor data
# threading.Thread(target=motor_data_simulation).start()
threading.Thread(target=arduino_connect.start_stuff, 
                 args=(socketio,q,), daemon=True).start()

if __name__ == "__main__":
    socketio.run(app, use_reloader=False)