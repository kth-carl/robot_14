

// Variabels
verbose = true

// Constants
var socket = io();
var rpm = 0;
let timestamp = Date.now();
let motor_data = null;


// Motor power slider
let slider_mode = "input" // "change" only on input, else use "input"
let rpm_slider = document.getElementById('rpm_slider');
let rpm_slider_text = document.getElementById("rpm_slider_text");
let rpm_cell = document.getElementById("actual_rpm_cell");
let socket_connction_status = document.getElementById("socket_connction_status")
let start_motor_button = document.getElementById("start_motor")
let stop_motor_button = document.getElementById("stop_motor")


// Add event listener on the power_to_moter slider
rpm_slider.addEventListener(slider_mode, event_update_rpm);


function event_update_rpm(e) {

    let rpm = e.target.value
    updateValue(rpm)
    
}


// Maybe check timestamp to ignore old instruction in "input" mode
function updateValue(rpm, request_data=true) {

    if (rpm == undefined) {
        return;
    }
    
    if (verbose == true) {
        console.log("Motor Value: " + rpm)
    }

    // Request new data
    if (request_data == true) {
        socket.emit('send_motor_power', rpm);
    }


    // Start / Stop buttons
    // Motor is OFF
    if (rpm  == 0) {

        motor_status_cell.style.color = "red";
        motor_status_cell.innerHTML = "AV";

        start_motor_button.disabled =  false
        stop_motor_button.disabled =  true

    // Motor is ON
    } else {

        motor_status_cell.style.color = "green";
        motor_status_cell.innerHTML = "PÅ";

        start_motor_button.disabled =  true
        stop_motor_button.disabled =  false
    }

    // RPM Cell
    rpm_cell.innerHTML = rpm;

    // Update the slider value
    rpm_slider.value = rpm

    // Set rpm value
    rpm_slider_text.innerHTML = "Motorhastighet (RPM): " + rpm


}


// Socket connection
socket.on('connect', function () {
    console.log('Connected | ' + new Date());
    socket.emit("init_connect")
});


// Set the socket connection status
socket.on('disconnect', function() {

    // If we are offline
    rpm = 0

    // Server is offline --> Dont request any data
    updateValue(rpm, request_data=false)

    socket_connction_status.innerHTML = "Offline"

});


socket.on('init_connect_data', function(data){

    if (verbose == true) {
        console.log("Init connect!")

        console.log(data["rpm"])
    }

    rpm = data["rpm"]
    timestamp = data["timestamp"]


    // We already recive data with init connect
    updateValue(rpm, "init", request_data=false);

    // Set connection status as ONLINE
    socket_connction_status.innerHTML = "Online"
    
    
});


function update_motor_data(data) {
    /*
    Update the motor_data in the tabel
    */

    rpm = data["rpm"]
    timestamp = data["timestamp"]

    updateValue(rpm, request_data=false);

}


// Get motor_data
socket.on('request_motor_data', function (data) {

    // Set motor_data 
    motor_data = data

    // Lets update the innerHTMLs in the tabel
    update_motor_data(motor_data)

    // Plot
    plot_motor_data(motor_data)

});


socket.on('recive_motor_power', function (data) {

    rpm = data["rpm"];
    updateValue(data)

});
