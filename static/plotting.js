
var trace1 = {
    x: [],
    y: [],
    mode: 'lines',
    type: 'scatter',
    line: {
        color: '#80CAF6',
        shape: 'spline'
    },
    name: 'Motor styrka'
}


var layout = {
    showlegend: false,
    xaxis: {
        type: 'date',
        domain: [0, 1],
        showticklabels: false
    },
    yaxis: {
        domain: [0, 1],
        range: [0, 270],
        title: {
            text: 'Motorhastighet (RPM)',
            font: {
            //   family: 'Courier New, monospace',
              size: 18,
              color: '#7f7f7f'
            }
        }
    }
    
}

var config = {responsive: true}

var plot_data = [trace1];
Plotly.plot('tester2', plot_data, layout, config);

function plot_motor_data(data) {

    var time = new Date(data["timestamp"]);
    if (time < 1618047953900) {
        return;
    }

    var update = {
        x: [
            [time],
        ],
        y: [
            [data["rpm"]],
        ]
    }

    Plotly.extendTraces('tester2', update, [0])

}